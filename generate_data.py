def getRolesQuery():
	query = "INSERT INTO `so-net`.`roles` (id, code, title) VALUES \n" 
	query += " (1, '{0}', '{1}'), \n".format('ROLE_USER', 'User')
	query += " (2, '{0}', '{1}'); \n".format('ROLE_MODERATOR', 'Moderator')

	return query

def getUsersQuery(usersNum):
	query = "INSERT INTO `so-net`.`users` (id, username, email, password, salt, is_banned) VALUES \n"

	password = "Uqzhc7D00TlH9IE9JMFoXyK6ay5gIKai7RfkKjLWi1NZTR/FwoC4gCVAyW7hFDHnUcrt3l72Nxz3PPkv7OC9IQ=="
	salt = "pZtxUXBnTqhOTzj0"
	email = "user{0}@mail.ru"

	for i in range(1, usersNum + 1):
		query += " ({0}, '{1}', '{2}', '{3}', '{4}', 0), \n".format(i, "user" + str(i), email.format(i), password, salt)
		if i == usersNum:
			query = query[:-3] + ';'
	return query

def getRolesUsersQuery(usersNum):
	query = "INSERT INTO `so-net`.`users_roles` (user_id, role_id) VALUES \n" 
	for i in range(1, usersNum + 1):
		query += " ({0}, {1}), \n".format(i, 1)
		if i == usersNum:
			query = query[:-3] + ';'
	return query

def getFriendshipQuery(rowsNum):
	query = "INSERT INTO `so-net`.`friendship` (user_id, friend_id) VALUES \n" 
	group_size = 101

	first_in_group = 1
	for user_id in range(1, usersNum + 1):
		if user_id % (group_size + 1) == 0:
			first_in_group = user_id

		for friend_id in range(first_in_group, first_in_group + group_size):
			if friend_id > usersNum:
				break
			if (user_id == friend_id):
				continue
			query += " ({0}, {1}), \n".format(user_id, friend_id)

		if user_id == usersNum:
			query = query[:-3] + ';'
	return query

def getPostsQuery(userPostsNum):
	query = "INSERT INTO `so-net`.`posts` (user_id, content, created_at) VALUES \n" 

	content = 'Post{0} bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac cursus commodo, tor.'
	post_id = 1
	for i in range(1, usersNum + 1):
		for j in range(userPostsNum):
			query += " ({0}, '{1}', '{2}'), \n".format(i, content.format(post_id), '2015-01-01 00:00:01')
			post_id += 1
	if i == usersNum:
		query = query[:-3] + ';'
	return query

f = open('dump.sql', 'w')
usersNum = 10000

f.write(getRolesQuery())
f.write(getUsersQuery(usersNum))
f.write(getRolesUsersQuery(usersNum))
f.write(getFriendshipQuery(usersNum))
#f.write(getPostsQuery(10))

