<?php

namespace Artvisio\SoNetBundle\Model;


use Artvisio\SoNetBundle\Entity\User;
use Artvisio\SoNetBundle\Helper\RandomGenerator;
use Doctrine\ORM\EntityManager;

class UserModel
{
    protected $em;
    protected $encoder;
    protected $paginator;

    public function __construct(EntityManager $em, $paginator, $encoder)
    {
        $this->em = $em;
        $this->paginator = $paginator;
        $this->encoder = $encoder;
    }

    public function createUser($data)
    {
        $userRole = $this->em->getRepository('SoNetBundle:Role')->findOneByCode('ROLE_USER');

        $user = new User();
        $user->setUsername($data['username']);
        $user->setEmail($data['email']);

        $salt = RandomGenerator::generateRandomString();
        $user->setSalt($salt);
        $user->setPassword($this->encoder->encodePassword($user, $data['password']));
        $user->addRole($userRole);

        $this->em->persist($user);
        $this->em->flush();

        return $user;
    }

    public function getMatchedUsers($data)
    {
        return $this->em->getRepository('SoNetBundle:User')->findMatchedUsers($data['username']);
    }

    public function getMatchedUsersPaginator($data, $page = 1, $limit = 10)
    {
        $qb = $this->em->getRepository('SoNetBundle:User')->getFindMatchedUsersQuery($data['username']);

        return $this->paginator->paginate($qb, $page, $limit);
    }

    public function banUser($user)
    {
        $user->setIsBanned(true);
        $this->em->flush();

        return true;
    }

    public function unbanUser($user)
    {
        $user->setIsBanned(false);
        $this->em->flush();

        return true;
    }

    public function getFriendsPaginator($user, $page = 1, $limit = 10)
    {
        $qb = $this->em->getRepository('SoNetBundle:User')->getFindFriendsQuery($user);

        return $this->paginator->paginate($qb, $page, $limit);
    }

}