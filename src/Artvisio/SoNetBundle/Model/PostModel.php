<?php

namespace Artvisio\SoNetBundle\Model;


use Artvisio\SoNetBundle\Entity\Post;
use Doctrine\ORM\EntityManager;

class PostModel
{
    protected $em;
    protected $paginator;

    public function __construct(EntityManager $em, $paginator)
    {
        $this->em = $em;
        $this->paginator = $paginator;
    }

    public function create($user, $data)
    {
        $post = new Post();
        $post->setUser($user);
        $post->setCreatedAt(new \DateTime());
        $post->setContent($data['content']);

        $this->em->persist($post);
        $this->em->flush();

        return $post;
    }

    public function getUserPosts($user)
    {
        return $this->em->getRepository('SoNetBundle:Post')->findBy(array('user' => $user), array('createdAt' => 'DESC'));
    }

    public function edit($post)
    {
        $this->em->flush();

        return $post;
    }

    public function getUserPostsPaginator($user, $page = 1, $limit = 10)
    {
        $userIds = $this->em->getRepository('SoNetBundle:User')->findFriendIdsByUser($user);
        $userIds[] = $user->getId();

        $qb = $this->em->getRepository('SoNetBundle:Post')->getFindPostByUsersQuery($userIds);

        return $this->paginator->paginate($qb, $page, $limit);
    }
}