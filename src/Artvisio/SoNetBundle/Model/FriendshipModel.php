<?php

namespace Artvisio\SoNetBundle\Model;


use Artvisio\SoNetBundle\Constants;
use Artvisio\SoNetBundle\Entity\FriendshipRequest;
use Doctrine\ORM\EntityManager;
use Artvisio\SoNetBundle\Entity\User;
use Doctrine\ORM\EntityNotFoundException;

class FriendshipModel
{
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param User $user
     * @param User $friend
     * @return bool
     */
    public function removeFriend($user, $friend)
    {
        $this->em->beginTransaction();
        try {
            $user->removeFriend($friend);
            $friend->removeFriend($user);

            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();

            return false;
        }

        return true;
    }

    public function addFriend(User $user, $data)
    {
        $this->em->beginTransaction();
        try {
            $friend = $this->em->getRepository('SoNetBundle:User')->findSimpleUserByUsername($data['username']);
            if (!$friend || $user->getFriends()->contains($friend) || $user == $friend) {
                throw new EntityNotFoundException('User not found');
            }

            $friendRequest = $this->em->getRepository('SoNetBundle:FriendshipRequest')
                ->findOneBy(array('sender' => $user, 'recipient' => $friend, 'status' => Constants::FRIENDSHIP_STATUS_PENDING));
            if ($friendRequest) {
                return $friend;
            }

            $friendRequest = new FriendshipRequest();
            $friendRequest->setSender($user);
            $friendRequest->setRecipient($friend);
            $friendRequest->setStatus(Constants::FRIENDSHIP_STATUS_PENDING);
            $this->em->persist($friendRequest);

            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            throw $e;
        }

        return $friend;
    }

    public function getReceivedFriendRequests($user)
    {
        return $this->em->getRepository('SoNetBundle:FriendshipRequest')->findReceived($user);
    }

    public function confirmFriendRequest($friendRequest)
    {
        $this->em->beginTransaction();
        try {
            $recipient = $friendRequest->getRecipient();
            $sender = $friendRequest->getSender();
            $recipient->addFriend($sender);
            $sender->addFriend($recipient);

            $friendRequest->setStatus(Constants::FRIENDSHIP_STATUS_ACCEPTED);

            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            return false;
        }

        return true;
    }

    public function declineFriendRequest($friendRequest)
    {
        $this->em->beginTransaction();
        try {
            $friendRequest->setStatus(Constants::FRIENDSHIP_STATUS_DECLINED);

            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollback();
            return false;
        }

        return true;
    }
}