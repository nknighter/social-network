<?php

namespace Artvisio\SoNetBundle\Controller;

use Artvisio\SoNetBundle\Entity\FriendshipRequest;
use Artvisio\SoNetBundle\Entity\User;
use Artvisio\SoNetBundle\Form\Frienship\AddFriendForm;
use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FriendshipController extends Controller
{
    /**
     * @Route("/friends", name="_friends_index")
     * @Security("has_role('ROLE_USER')")
     */
    public function friendListAction(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm(new AddFriendForm());
        $form->handleRequest($request);
        if ($form->isValid()) {
            try {
                $this->get('model.friendship')->addFriend($user, $form->getData());
                $this->addFlash('info', 'request has sent!');
            } catch (EntityNotFoundException $e) {
                $this->addFlash('error', 'User not found');
            }
            catch (\Exception $e) {
                $this->addFlash('error', 'Error.');
            }
        }

        $page = $request->get('page') ? $request->get('page') : 1;
        $paginator = $this->get('model.user')->getFriendsPaginator($user, $page);

        return $this->render('SoNetBundle:Friendship:list.html.twig', array(
            'friends' => $paginator,
            'form' => $form->createView(),
            'pagination' => $paginator
        ));
    }

    /**
     * @Route("/friends/{username}/delete", name="_friends_delete")
     * @ParamConverter("friend", class="SoNetBundle:User")
     * @Security("has_role('ROLE_USER')")
     */
    public function deleteFriendAction(User $friend)
    {
        $user = $this->getUser();

        if (!$user->getFriends()->contains($friend)) {
            $this->addFlash('error', 'User not found in your friends');

            return $this->redirectToRoute('_friends_index');
        }

        $status = $this->get('model.friendship')->removeFriend($user, $friend);

        if ($status) {
            $this->addFlash('info', $friend->getUsername() . ' successfully removed from friend list');
        } else {
            $this->addFlash('error', 'Something gone wrong');
        }

        return $this->redirectToRoute('_friends_index');
    }

    /**
     * @Route("/friend-requests", name="_friend_requests")
     * @Security("has_role('ROLE_USER')")
     */
    public function requestListAction()
    {
        $user = $this->getUser();

        $friendRequests = $this->get('model.friendship')->getReceivedFriendRequests($user);

        return $this->render('SoNetBundle:Friendship:requests.html.twig', array(
            'requests' => $friendRequests,
        ));
    }

    /**
     * @Route("/friend-requests/{id}/confirm", name="_friend_request_confirm")
     * @ParamConverter("friendRequest", class="SoNetBundle:FriendshipRequest")
     * @Security("has_role('ROLE_USER')")
     */
    public function confirmFriendRequestAction(FriendshipRequest $friendRequest)
    {
        $user = $this->getUser();
        if ($friendRequest->getRecipient() !== $user) {
            return $this->createAccessDeniedException();
        }

        $status = $this->get('model.friendship')->confirmFriendRequest($friendRequest);
        if ($status) {
            $this->addFlash('info', 'Successfully confirmed!');
        } else {
            $this->addFlash('error', 'Something gone wrong');
        }

        return $this->redirectToRoute('_friends_index');

    }

    /**
     * @Route("/friend-requests/{id}/decline", name="_friend_request_decline")
     * @ParamConverter("friendRequest", class="SoNetBundle:FriendshipRequest")
     * @Security("has_role('ROLE_USER')")
     */
    public function declineFriendRequestAction(FriendshipRequest $friendRequest)
    {
        $user = $this->getUser();
        if ($friendRequest->getRecipient() !== $user) {
            return $this->createAccessDeniedException();
        }

        $status = $this->get('model.friendship')->declineFriendRequest($friendRequest);
        if ($status) {
            $this->addFlash('info', 'Successfully declined!');
        } else {
            $this->addFlash('error', 'Something gone wrong');
        }

        return $this->redirectToRoute('_friends_index');

    }
}