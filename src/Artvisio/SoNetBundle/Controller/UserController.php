<?php

namespace Artvisio\SoNetBundle\Controller;


use Artvisio\SoNetBundle\Entity\User;
use Artvisio\SoNetBundle\Form\User\FindUsersForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    /**
     * @Route("/users/{username}", name="_user_index")
     * @ParamConverter("user", class="SoNetBundle:User")
     * @Security("has_role('ROLE_USER')")
     */
    public function indexAction(User $user, Request $request)
    {
        $page = $request->get('page') ? $request->get('page') : 1;
        $paginator = $this->get('model.post')->getUserPostsPaginator($user, $page);

        return $this->render('SoNetBundle:User:index.html.twig', array(
            'user' => $user,
            'posts' => $paginator,
            'pagination' => $paginator
        ));
    }

    /**
     * @Route("/moderators/home", name="_moderator_index")
     * @Security("has_role('ROLE_MODERATOR')")
     */
    public function moderatorIndexAction(Request $request)
    {
        $form = $this->createForm(new FindUsersForm());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $users = $this->get('model.user')->getMatchedUsers($form->getData());
        }

        return $this->render('SoNetBundle:User:moderator_index.html.twig', array(
            'form' => $form->createView(),
            'users' => isset($users) ? $users : false,
        ));
    }

    /**
     * @Route("/users/{id}/ban", name="_user_ban")
     * @ParamConverter("user", class="SoNetBundle:User")
     * @Security("has_role('ROLE_MODERATOR')")
     */
    public function banUserAction(User $user)
    {
        $status = $this->get('model.user')->banUser($user);
        if ($status) {
            $this->addFlash('info', $user->getUsername() . ' has banned');
        } else {
            $this->addFlash('error', 'Error');
        }

        return $this->redirectToRoute('_moderator_index');
    }

    /**
     * @Route("/users/{id}/unban", name="_user_unban")
     * @ParamConverter("user", class="SoNetBundle:User")
     * @Security("has_role('ROLE_MODERATOR')")
     */
    public function unbanUserAction(User $user)
    {
        $status = $this->get('model.user')->unbanUser($user);
        if ($status) {
            $this->addFlash('info', $user->getUsername() . ' has unbanned');
        } else {
            $this->addFlash('error', 'Error');
        }

        return $this->redirectToRoute('_moderator_index');
    }

}