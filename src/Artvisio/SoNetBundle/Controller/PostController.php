<?php

namespace Artvisio\SoNetBundle\Controller;

use Artvisio\SoNetBundle\Entity\Post;
use Artvisio\SoNetBundle\Form\Post\CreatePostForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    /**
     * @Route("/post/create", name="_post_create")
     * @Security("has_role('ROLE_USER')")
     */
    public function createAction(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm(new CreatePostForm());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $post = $this->get('model.post')->create($user, $form->getData());

            $this->addFlash('info', 'Post successfully created');
            return $this->redirectToRoute('_user_index', array('username' => $user->getUsername()));
        }

        return $this->render('SoNetBundle:Post:create.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/post/{id}/edit", name="_post_edit")
     * @ParamConverter("post", class="SoNetBundle:Post")
     * @Security("has_role('ROLE_MODERATOR')")
     */
    public function editAction(Post $post, Request $request)
    {
        $form = $this->createForm(new CreatePostForm(), $post);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $post = $this->get('model.post')->edit($form->getData());

            $this->addFlash('info', 'Post successfully edited');
            return $this->redirectToRoute('_user_index', array('username' => $post->getUser()->getUsername()));
        }

        return $this->render('SoNetBundle:Post:edit.html.twig', array(
            'form' => $form->createView(),
        ));

    }
}
