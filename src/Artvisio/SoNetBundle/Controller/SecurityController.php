<?php

namespace Artvisio\SoNetBundle\Controller;


use Artvisio\SoNetBundle\Form\SignUpForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends Controller
{
    /**
     * @Route("/login", name="_login")
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        // get the login error if there is one
        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(
                Security::AUTHENTICATION_ERROR
            );
        } elseif (null !== $session && $session->has(Security::AUTHENTICATION_ERROR)) {
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        } else {
            $error = '';
        }

        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get(Security::LAST_USERNAME);

        return $this->render(
            'SoNetBundle:Security:login.html.twig',
            array(
                // last username entered by the user
                'last_username' => $lastUsername,
                'error'         => $error,
            )
        );
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function loginCheckAction()
    {

    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {

    }

    /**
     * @Route("/sign-up", name="_sign_up")
     */
    public function signUpAction(Request $request)
    {
        $form = $this->createForm(new SignUpForm());

        $form->handleRequest($request);
        if ($form->isValid()) {
            $user = $this->get('model.user')->createUser($form->getData());

            $token = new UsernamePasswordToken($user, $user->getPassword(), 'main', $user->getRoles());
            $request->getSession()->set('_security_main', serialize($token));

            $this->addFlash('info', 'Signed up successfully!');

            return $this->redirectToRoute('_user_index', array('username' => $user->getUsername()));
        }

        return $this->render('SoNetBundle:Security:sign_up.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}