<?php

namespace Artvisio\SoNetBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="Artvisio\SoNetBundle\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable, AdvancedUserInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=25, unique=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=60, unique=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=32)
     */
    private $salt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_banned", type="boolean")
     */
    private $isBanned;

    /**
     * @ORM\ManyToMany(targetEntity="Role")
     * @ORM\JoinTable(name="users_roles")
     */
    private $roles;

    /**
     * @ORM\ManyToMany(targetEntity="User", inversedBy="friendsWithMe")
     * @ORM\JoinTable(name="friendship",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="friend_id", referencedColumnName="id")}
     *      )
     **/
    private $friends;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="friends")
     **/
    private $friendsWithMe;

    /**
     * @ORM\OneToMany(targetEntity="Artvisio\SoNetBundle\Entity\FriendshipRequest", mappedBy="sender")
     */
    private $sendFriendRequests;

    /**
     * @ORM\OneToMany(targetEntity="Artvisio\SoNetBundle\Entity\FriendshipRequest", mappedBy="recipient")
     */
    private $receivedFriendRequests;

    /**
     * @ORM\OneToMany(targetEntity="Artvisio\SoNetBundle\Entity\Post", mappedBy="user")
     */
    private $posts;

    public function __construct()
    {
        $this->isBanned = false;
        $this->roles = new ArrayCollection();
        $this->friends = new ArrayCollection();
        $this->receivedFriendRequests = new ArrayCollection();
        $this->sendFriendRequests = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set isBanned
     *
     * @param boolean $isBanned
     * @return User
     */
    public function setIsBanned($isBanned)
    {
        $this->isBanned = $isBanned;

        return $this;
    }

    /**
     * Get isBanned
     *
     * @return boolean
     */
    public function getIsBanned()
    {
        return $this->isBanned;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
            $this->isBanned
        ));
    }


    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password,
            $this->salt,
            $this->isBanned
            ) = unserialize($serialized);
    }

    public function getRoles()
    {
        return array_map(
            function (Role $role) {
                return $role->getCode();
            },
            $this->roles->toArray()
        );
    }

    public function eraseCredentials()
    {

    }


    /**
     * Add roles
     *
     * @param Role $roles
     * @return User
     */
    public function addRole(Role $roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Remove roles
     *
     * @param Role $roles
     */
    public function removeRole(Role $roles)
    {
        $this->roles->removeElement($roles);
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);

        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }

        return $randomString;
    }

    /**
     * Add sendFriendRequests
     *
     * @param \Artvisio\SoNetBundle\Entity\FriendshipRequest $sendFriendRequests
     * @return User
     */
    public function addSendFriendRequest(\Artvisio\SoNetBundle\Entity\FriendshipRequest $sendFriendRequests)
    {
        $this->sendFriendRequests[] = $sendFriendRequests;

        return $this;
    }

    /**
     * Remove sendFriendRequests
     *
     * @param \Artvisio\SoNetBundle\Entity\FriendshipRequest $sendFriendRequests
     */
    public function removeSendFriendRequest(\Artvisio\SoNetBundle\Entity\FriendshipRequest $sendFriendRequests)
    {
        $this->sendFriendRequests->removeElement($sendFriendRequests);
    }

    /**
     * Get sendFriendRequests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSendFriendRequests()
    {
        return $this->sendFriendRequests;
    }

    /**
     * Add receivedFriendRequests
     *
     * @param \Artvisio\SoNetBundle\Entity\FriendshipRequest $receivedFriendRequests
     * @return User
     */
    public function addReceivedFriendRequest(\Artvisio\SoNetBundle\Entity\FriendshipRequest $receivedFriendRequests)
    {
        $this->receivedFriendRequests[] = $receivedFriendRequests;

        return $this;
    }

    /**
     * Remove receivedFriendRequests
     *
     * @param \Artvisio\SoNetBundle\Entity\FriendshipRequest $receivedFriendRequests
     */
    public function removeReceivedFriendRequest(\Artvisio\SoNetBundle\Entity\FriendshipRequest $receivedFriendRequests)
    {
        $this->receivedFriendRequests->removeElement($receivedFriendRequests);
    }

    /**
     * Get receivedFriendRequests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReceivedFriendRequests()
    {
        return $this->receivedFriendRequests;
    }

    /**
     * Add friends
     *
     * @param \Artvisio\SoNetBundle\Entity\User $friends
     * @return User
     */
    public function addFriend(\Artvisio\SoNetBundle\Entity\User $friends)
    {
        $this->friends[] = $friends;

        return $this;
    }

    /**
     * Remove friends
     *
     * @param \Artvisio\SoNetBundle\Entity\User $friends
     */
    public function removeFriend(\Artvisio\SoNetBundle\Entity\User $friends)
    {
        $this->friends->removeElement($friends);
    }

    /**
     * Get friends
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFriends()
    {
        return $this->friends;
    }

    /**
     * Add posts
     *
     * @param \Artvisio\SoNetBundle\Entity\Post $posts
     * @return User
     */
    public function addPost(\Artvisio\SoNetBundle\Entity\Post $posts)
    {
        $this->posts[] = $posts;

        return $this;
    }

    /**
     * Remove posts
     *
     * @param \Artvisio\SoNetBundle\Entity\Post $posts
     */
    public function removePost(\Artvisio\SoNetBundle\Entity\Post $posts)
    {
        $this->posts->removeElement($posts);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isBanned === false;
    }


    /**
     * Add friendsWithMe
     *
     * @param \Artvisio\SoNetBundle\Entity\User $friendsWithMe
     * @return User
     */
    public function addFriendsWithMe(\Artvisio\SoNetBundle\Entity\User $friendsWithMe)
    {
        $this->friendsWithMe[] = $friendsWithMe;

        return $this;
    }

    /**
     * Remove friendsWithMe
     *
     * @param \Artvisio\SoNetBundle\Entity\User $friendsWithMe
     */
    public function removeFriendsWithMe(\Artvisio\SoNetBundle\Entity\User $friendsWithMe)
    {
        $this->friendsWithMe->removeElement($friendsWithMe);
    }

    /**
     * Get friendsWithMe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFriendsWithMe()
    {
        return $this->friendsWithMe;
    }
}
