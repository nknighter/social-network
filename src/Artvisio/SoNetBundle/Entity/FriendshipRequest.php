<?php

namespace Artvisio\SoNetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FriendshipRequest
 *
 * @ORM\Table(name="friendship_requests")
 * @ORM\Entity(repositoryClass="Artvisio\SoNetBundle\Repository\FriendRequestRepository")
 */
class FriendshipRequest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Artvisio\SoNetBundle\Entity\User", inversedBy="sendFriendRequests")
     * @ORM\JoinColumn(name="sender_id", referencedColumnName="id")
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity="Artvisio\SoNetBundle\Entity\User", inversedBy="receivedFriendRequests")
     * @ORM\JoinColumn(name="recipient_id", referencedColumnName="id")
     */
    private $recipient;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return FriendshipRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set sender
     *
     * @param \Artvisio\SoNetBundle\Entity\User $sender
     * @return FriendshipRequest
     */
    public function setSender(\Artvisio\SoNetBundle\Entity\User $sender = null)
    {
        $this->sender = $sender;

        return $this;
    }

    /**
     * Get sender
     *
     * @return \Artvisio\SoNetBundle\Entity\User
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Set recipient
     *
     * @param \Artvisio\SoNetBundle\Entity\User $recipient
     * @return FriendshipRequest
     */
    public function setRecipient(\Artvisio\SoNetBundle\Entity\User $recipient = null)
    {
        $this->recipient = $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return \Artvisio\SoNetBundle\Entity\User
     */
    public function getRecipient()
    {
        return $this->recipient;
    }
}
