<?php

namespace Artvisio\SoNetBundle;


class Constants
{
    const FRIENDSHIP_STATUS_PENDING = 1;
    const FRIENDSHIP_STATUS_ACCEPTED = 2;
    const FRIENDSHIP_STATUS_DECLINED = 3;
}