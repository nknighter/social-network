<?php

namespace Artvisio\SoNetBundle\Form\Post;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreatePostForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('content', 'textarea', array(
            'required' => false,
            'constraints' => array(
                new NotBlank(array('message' => 'Обязательное поле')),
                new Length(array('max' => 140, 'maxMessage' => 'Максимум {{ limit }} символов'))
            )
        ));
    }

    public function getName()
    {
        return 'post';
    }
}