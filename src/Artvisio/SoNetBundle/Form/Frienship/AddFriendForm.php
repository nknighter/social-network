<?php


namespace Artvisio\SoNetBundle\Form\Frienship;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class AddFriendForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'text', array(
            'required' => false,
            'constraints' => array(
                new NotBlank(array('message' => 'Обязательное поле')),
                new Length(array('min' => 2, 'max' => 25, 'minMessage' => 'Минимум {{ limit }} символа', 'maxMessage' => 'Максимум {{ limit }} символа'))
            )
        ));
    }

    public function getName()
    {
        return 'add_friend';
    }
}