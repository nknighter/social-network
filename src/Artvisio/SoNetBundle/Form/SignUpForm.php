<?php

namespace Artvisio\SoNetBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class SignUpForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'text', array(
            'label' => 'Имя пользователя',
            'required' => false,
            'constraints' => array(
                new NotBlank(array('message' => 'Обязательное поле')),
                new Length(array('min' => 2, 'max' => 25, 'minMessage' => 'Минимум {{ limit }} символа', 'maxMessage' => 'Максимум {{ limit }} символа'))
            )
        ));

        $builder->add('email', 'email', array(
            'label' => 'Email',
            'required' => false,
            'constraints' => array(
                new Email(array('message' => 'Неверный email')),
                new NotBlank(array('message' => 'Обязательное поле'))
            )
        ));

        $builder->add('password', 'repeated', array(
            'type' => 'password',
            'invalid_message' => 'Пароли должны совпадать',
            'required' => false,
            'first_options'  => array(
                'label' => 'Пароль',
                'constraints' => array(
                    new NotBlank(array('message' => 'Обязательное поле')),
                    new Length(array('min' => 2, 'max' => 25, 'minMessage' => 'Минимум {{ limit }} символа', 'maxMessage' => 'Максимум {{ limit }} символа'))
                )
            ),
            'second_options' => array(
                'label' => 'Подтвердите пароль',
                'constraints' => array(
                    new NotBlank(array('message' => 'Обязательное поле')),
                    new Length(array('min' => 2, 'max' => 25, 'minMessage' => 'Минимум {{ limit }} символа', 'maxMessage' => 'Максимум {{ limit }} символа'))
                )
            ),
        ));
    }

    public function getName()
    {
        return 'sign_up';
    }

}