$(function () {

    // Remove flash message after 5 sec
    $(".flash-info, .flash-error").delay(5000).fadeOut('slow');

});