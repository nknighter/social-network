<?php

namespace Artvisio\SoNetBundle\Repository;


use Artvisio\SoNetBundle\Constants;
use Doctrine\ORM\EntityRepository;

class FriendRequestRepository extends EntityRepository
{
    public function findReceived($user)
    {
        $qb = $this
            ->createQueryBuilder('r')
            ->addSelect('rs')
            ->join('r.sender', 'rs')
            ->where('r.status = :status')
            ->andWhere('r.recipient = :user')
            ->setParameter('user', $user)
            ->setParameter('status', Constants::FRIENDSHIP_STATUS_PENDING);

        return $qb->getQuery()->getResult();
    }
}