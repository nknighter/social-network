<?php

namespace Artvisio\SoNetBundle\Repository;


use Doctrine\ORM\EntityRepository;

class PostRepository extends EntityRepository
{
    public function getFindPostByUsersQuery($userIds)
    {
        $qb = $this->createQueryBuilder('p')
            ->join('p.user', 'u')
            ->where('u.id IN (:user_ids)')
            ->orderBy('p.createdAt', 'DESC')
            ->setParameter('user_ids', $userIds);

        return $qb;
    }
}