<?php

namespace Artvisio\SoNetBundle\Repository;


use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{

    public function findSimpleUserByUsername($username)
    {
        $qb = $this
            ->createQueryBuilder('u')
            ->join('u.roles', 'r')
            ->where('u.username = :username')
            ->andWhere('r.code = :user_role')
            ->setParameter('username', $username)
            ->setParameter('user_role', 'ROLE_USER');

        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findMatchedUsers($username)
    {
        $qb = $this->getFindMatchedUsersQuery($username);

        return $qb->getQuery()->getResult();
    }

    public function getFindMatchedUsersQuery($username)
    {
        $qb = $this
            ->createQueryBuilder('u')
            ->join('u.roles', 'r')
            ->where('u.username LIKE :username')
            ->andWhere('r.code = :user_role')
            ->setParameter('username', '%' . $username . '%')
            ->setParameter('user_role', 'ROLE_USER');

        return $qb;
    }

    public function getFindFriendsQuery($user)
    {
        $qb = $this
            ->createQueryBuilder('u')
            ->join('u.friends', 'f')
            ->where('f = :user')
            ->setParameter('user', $user);

        return $qb;
    }

    public function findFriendIdsByUser($user)
    {
        $q = $this->_em->createQuery("
            SELECT u.id
            FROM SoNetBundle:User u
            JOIN u.friends f
            WHERE f = :user
        ")
            ->setParameter('user', $user);

        return $q->getResult();
    }
}